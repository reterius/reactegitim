<?php

	$dsn = 'mysql:dbname=test;host=localhost';
	$kullanici = 'root';
	$sifre = '';
	$vt = new PDO($dsn, $kullanici, $sifre);
	$vt->exec("SET NAMES 'utf8'; SET CHARSET 'utf8'");

	/* aşağıda bu kodun kullanımı açıklanacaktır. */
	$inputJSON = file_get_contents('php://input');
	$data = json_decode($inputJSON, TRUE);




	$islem = $data['islem'];

	if ($islem == "deletePersonel") {

		$PersonelID = $data['PersonelID'];


		$sorgu = $vt->prepare("DELETE FROM kisiler WHERE id = ?");
		$sorgu->bindValue(1, $PersonelID, PDO::PARAM_INT);

		$sorgu->execute();
	} elseif ($islem == "editPersonel") {
		$PersonelID = $data['PersonelID'];



		$sorgu = $vt->query("select * from kisiler  where id = " . $PersonelID . " limit 0,1");



		$list = $sorgu->fetchAll();
		$Personel = $list[0];

		echo json_encode($Personel);
	} elseif ($islem == "updatePersonel") {
		$ad = $data['ad'];
		$soyad = $data['soyad'];
		$tc = $data['tc'];
		$id = $data['id'];

		$sorgu = $vt->prepare("update kisiler set ad = ?, soyad = ?, tc = ? where id = ?");
		$sorgu->bindValue(1, $ad, PDO::PARAM_STR);
		$sorgu->bindValue(2, $soyad, PDO::PARAM_STR);
		$sorgu->bindValue(3, $tc, PDO::PARAM_STR);
		$sorgu->bindValue(4, $id, PDO::PARAM_INT);
		$sorgu->execute();
	}
?>
<?php
	
	
$dsn = 'mysql:dbname=test;host=localhost';
$kullanici = 'root';
$sifre = '';
$vt = new PDO($dsn, $kullanici, $sifre);	
$vt->exec("SET NAMES 'utf8'; SET CHARSET 'utf8'");

/*aşağıda bu kodun kullanımı açıklanacaktır.*/
$inputJSON = file_get_contents('php://input'); 
$data= json_decode( $inputJSON, TRUE )	 ;
	
$ad = $data['ad'];	
$soyad = $data['soyad'];	
$tc = $data['tc'];	

$sorgu=$vt->prepare("insert into kisiler values(NULL,?,?,?)");
$sorgu->bindValue(1,$ad,PDO::PARAM_STR);
$sorgu->bindValue(2,$soyad,PDO::PARAM_STR);
$sorgu->bindValue(3,$tc,PDO::PARAM_STR);
$sorgu->execute(); 	
	
	
?>